<?php
// 这个页面处理删除任务
// 对于 GET 请求显示确认表单
// 对于 POST 请求执行任务

$page_title = "删除用户";
include 'template/header.php';

if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
	$id = $_GET["id"];
} else if (isset($_POST["id"]) && is_numeric($_POST["id"])) {
	$id = $_POST["id"];
} else {
	echo '<h1>非法的访问</h1>';
	echo '<p class="error">请不要这样访问此页面。</p>';
	include 'template/footer.php';
	exit();
}

echo '<h1>删除用户</h1>';

require_once 'mysqli_connect.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if ($_POST['sure'] == '是') {
		$q = "DELETE FROM users WHERE user_id=$id LIMIT 1";
		$r = @mysqli_query($dbc, $q);
		if (mysqli_affected_rows($dbc) == 1) {
			echo '<p>已成功删除用户。</p>';
		} else {
			echo '<h1>系统错误</h1>';
			echo '<p class="error">无法删除该用户。</p>';
			echo '<br /><br />指令：'."$q";
		}
	} else {
		echo '<p>删除已撤消。</p>';
	}
} else {
	$q = "SELECT CONCAT(last_name, ' ', first_name) as name FROM users WHERE user_id=$id";
	$r = @mysqli_query($dbc, $q);
	if (mysqli_num_rows($r) == 1) {
		$row = mysqli_fetch_row($r);
		$name = $row[0];
		echo '<p class="error">确认删除：'."$name".'</p>';
		echo '<p>您确定要删除这位用户吗？</p>';
		echo '<form action="delete_user.php" method="post">
			  <input type="radio" name="sure" value="是" />是
			  <input type="radio" name="sure" value="否" checked="checked" />否
			  <input type="submit" name="submit" value="提交" />
			  <input type="hidden" name="id" value="'.$id.'" /></form>';
	} else {
		echo '<h1>系统错误</h1>';
		echo '<p>找不到该用户。</p>';
	}
}
mysqli_close($dbc);
include 'template/footer.php';