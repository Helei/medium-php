<?php

// 根据目标页面的相对位置对网页进行重定向
function redirect($page = "index.php") {
	$url = "http://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"]);
	$url = rtrim($url, "/\\")."/$page";
	header("Location: $url\n");
	exit();
}

function check_login($dbc, $email, $pass) {
	$err = array();
	if (empty($email)) {
		$err[] = '未输入邮箱';
	} else {
		$em = mysqli_real_escape_string($dbc, $email);
	}
	if (empty($pass)) {
		$err[] = '未输入密码';
	} else {
		// 保存密码字串
		$p = mysqli_real_escape_string($dbc, trim($pass));
	}
	// 若信息已经完整输入，查询数据库
	if (empty($err)) {
		$q = "SELECT user_id, first_name FROM users WHERE email='$em' AND pass=SHA1('$pass')";
		$r = @mysqli_query($dbc, $q);
		// 正确时只有一条匹配记录
		if (mysqli_num_rows($r) != 1) {
			$err[] = "用户名或密码不正确";
		} else {
			$row = mysqli_fetch_assoc($r);
			return array($row, true);
		}
	}
	return array($err, false);
}