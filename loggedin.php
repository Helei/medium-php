<?php
session_start();
// 若用户在没有 cookie 的情况下访问页面，则重定向到主页
if (!isset($_SESSION["user_id"]) || !isset($_SESSION["first_name"]) ||
	!isset($_SESSION["AGENT"]) || md5($_SERVER["HTTP_USER_AGENT"]) != $_SESSION["AGENT"]) {
	include 'login_functions.php';
	print_r($_COOKIE);
	redirect();
}
session_regenerate_id();
$page_title = "登录成功";
include 'template/header.php';
echo '<h1>登录成功</h1>';
echo '<p>你好，'.$_SESSION["first_name"].'，您已经成功登录。</p>';
echo '<p><a href="logout.php">注销</a></p>';
include 'template/footer.php';