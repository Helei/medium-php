<!-- 网页头的模板 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 <html	xmlns="http://www.w3.org/1999/xhtml">
 <head>
 	 <title><?php echo $page_title ?></title>
 	 <link rel="stylesheet" type="text/css" href="css/style.css" />
 	 <meta http-equiv="content-type" content="text/html; charset=utf-8"	/>
 </head>
 <body>
	 <div id="header">
	 	 <h1>Dark Side of the Noodle</h1>
	 	 <h4>Dark side of the noodle is the best side of the noodle.</h4>
	 </div>
	 <div id="navigation">
	 	 <ul>
	 	 	 <li><a	href="index.php">主页</a></li>
	 	 	 <li><a	href="register.php">注册</a></li>
	 	 	 <li><a	href="view_users.php">查看用户</a></li>
	 	 	 <li><a	href="password.php">修改密码</a></li>
	 	 	 <?php if (isset($_SESSION["user_id"])): ?>
	 	 		<li><a href="logout.php">退出</a></li>
	 	 	 <?php else:?>
	 	 	 	<li><a href="login.php">登录</a></li>
	 	 	 <?php endif;?>
	 	 </ul>
	 </div>
	 <div id="content"><!-- Start of the page-specific content. -->