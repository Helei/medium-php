<?php
$page_title = "登录";
include 'template/header.php';

if (isset($errors) && !empty($errors)) {
	echo "<h1>登录失败</h1>";
	echo '<p class="error">发生了以下错误：</p>';
	foreach ($errors as $e) {
		echo ' - '.$e.'<br />';
	}
	echo "<br />请重试。";
}
?>
<h1>登录</h1>
<form action="login.php" method="POST">
	<p>电子邮箱：<input type=  "text" name="email" size="20" maxlength="60" /></p>
	<p>密码：<input type="password" name="pass" size= "20" maxlength="20" /></p>
	<p><input type="submit" name="submit" value="登录" /></p>
</form>
<?php include 'template/footer.php';?>
