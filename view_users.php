<?php
$page_title = "查看注册用户";
session_start();
include 'template/header.php';
echo '<h1>注册用户列表</h1>';
require 'mysqli_connect.php';

// 每页的最多显示用户数量
$PER_PAGE = 10;
// 深色列的背景
$bg = '#eeeeee';

// p 为所需的总页数
if (isset($_GET["p"]) && is_numeric($_GET["p"])) {
	$page = $_GET["p"];
} else {
	$q = "SELECT COUNT(user_id) FROM users";
	$r = @mysqli_query($dbc, $q);
	$row = mysqli_fetch_row($r);
	$total = $row[0];
	if ($total > $PER_PAGE) {
		$page = ceil($total / $PER_PAGE);
	} else {
		$page = 1;
	}
}

// s 为显示的起始点
if (isset($_GET["s"]) && is_numeric($_GET["s"])) {
	$start = $_GET["s"];
} else {
	$start = 0;
}

$sort = isset($_GET['sort']) ? $_GET["sort"] : 'rd';
switch ($sort) {
case 'fn':
	$order = 'ORDER BY first_name';
	break;
case 'ln':
	$order = 'ORDER BY last_name';
	break;
default:
case 'rd':
	$order = 'ORDER BY registration_date';
	break;
}

$q = "SELECT last_name, first_name,
	  DATE_FORMAT(registration_date, '%Y 年 %m 月 %d 日') AS rd,
	  user_id
	  FROM users $order
	  LIMIT $start, $PER_PAGE";

$r = mysqli_query($dbc, $q);
if ($r) {
	$num = mysqli_num_rows($r);
	if ($num > 0) {
		echo '<table align="center" cellpadding="3" width="75%">
			  <tr><td align="left"><b><a href="view_users.php?sort=ln">姓</a></b></td>
				  <td align="left"><b><a href="view_users.php?sort=fn">名</a></b></td>
			  	  <td align="left"><b><a href="view_users.php?sort=rd">注册日期</a></b></td>
			 	  <td align="left"><b>编辑</b></td>
				  <td align="left"><b>删除</b></td>
				</tr>';
		while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			$bg = ($bg == '#eeeeee') ? '#ffffff' : '#eeeeee';
			echo '<tr bgcolor="'.$bg.'"><td align="left">'.$row['last_name'].
			'</td><td align="left">'.$row['first_name'].'</td>
				  <td align="left">'.$row['rd'].
						'</td><td align="left"><a href="edit_user.php?id='.$row['user_id'].'">
				 		编辑</a></td><td align="left"><a href="delete_user.php?id='.$row['user_id'].'">
				 		删除</a></td></tr>';
		}
		echo '</table>';
		mysqli_free_result($r);
		// 若数据过多则启用分页
		if ($page > 1) {
			echo '<br /><p align="center">';
			$current = floor($start / $PER_PAGE) + 1;
			// 若不是第一页，则增加上一页的链接
			if ($current > 1) {
				echo '<a href="view_users.php?s='.($start - $PER_PAGE).'&p='.$page.'&sort='.$sort.'">
					   上一页</a>';
			}
			for ($i = 1; $i <= $page; $i++) {
				if ($i != $current) {
					echo '<a href="view_users.php?s='.
						(($PER_PAGE * ($i - 1))).'&p='.$page.'&sort='.$sort.'">'.$i.'</a>';
				} else {
					echo $i.' ';
				}
			}
			if ($current != $page) {
				echo '<a href="view_users.php?s='.($start + $PER_PAGE).'&p='.$page.'&sort='.$sort.'">
					   下一页</a>';
			}
			echo "</p>";
		}
	} else {
		echo '<p class="error">目前没有注册用户。</p>';
	}
} else {
	echo '<p class="error">抱歉，无法显示当前注册用户。</p>';
	echo '<p>'.mysqli_error($dbc).'<br /><br />指令：'.$q.'</p>';
}

mysqli_close($dbc);
include 'template/footer.php';
