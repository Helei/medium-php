<?php
// 配置文件里打开了 output_buffering=On 后，之前输出内容都可以被缓存，
// 并让 header 先被发出，使得 session 总是被正确设置
// echo '123123';
session_start();
// 如果接到了 POST 请求
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	require 'mysqli_connect.php';
	require 'login_functions.php';
	if (!isset($_POST["email"])) {
		$_POST["email"] = NULL;
	}
	if (!isset($_POST["pass"])) {
		$_POST["pass"] = NULL;
	}
	list($data, $check) = check_login($dbc, $_POST["email"], $_POST["pass"]);
	// 若登录成功
	if ($check) {
// 		setcookie("user_id", $data["user_id"]);
// 		setcookie("first_name", $data["first_name"]);
		$_SESSION["user_id"] = $data["user_id"];
		$_SESSION["first_name"] = $data["first_name"];
		// 将客户端配置作为客户端的标识符
		$_SESSION['AGENT'] = md5($_SERVER["HTTP_USER_AGENT"]);
		// 重定向到登录成功页面
		redirect("loggedin.php");
	} else {
		// 用于之后的错误显示
		$errors = $data;
	}
	mysqli_close($dbc);
}
require 'login_page.php';