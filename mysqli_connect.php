<?php

// constants for database connection
define ("DB_USR", "root");
define ("DB_PWD", "");
define ("DB_NAME", "sitename");
define ("DB_HOST", "localhost");

$dbc = @mysqli_connect(DB_HOST, DB_USR, DB_PWD, DB_NAME)
		 or die ("cannot connect to the database.".
		 		 mysqli_connect_error()."<br />");

// the encoding of the database will be the same as PHP source
mysqli_set_charset($dbc, "utf8");
