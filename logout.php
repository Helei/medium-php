<?php
session_start();
if (!isset($_SESSION["user_id"])) {
	require 'login_functions.php';
	redirect();
} else {
	$_SESSION = array();
	session_destroy();
	setcookie("PHPSESSID", '', time() - 3600);
	$page_title = "退出";
	include 'template/header.php';
	echo '<h1>退出成功</h1>';
	echo '<p>你好，您已经成功退出。</p>';
	include 'template/footer.php';
}

